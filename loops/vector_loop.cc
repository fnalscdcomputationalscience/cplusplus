#include <vector>

using std::vector;

double vector_loop(vector<double> const& v) {
  double result = 0.;
  for (auto i = 0UL, sz = v.size(); i != sz; ++i) {
    result += v[i];
  }
  return result;
}

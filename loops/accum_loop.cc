#include <vector>
#include <numeric>

using std::vector;

double accumulate_loop(vector<double> const& v) {
  return std::accumulate(begin(v), end(v), 0.0);
}

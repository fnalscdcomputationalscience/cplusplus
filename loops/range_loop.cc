#include <vector>
#include "range/v3/all.hpp"

using std::vector;

double range_loop(vector<double> const& v) {
  return ranges::accumulate(v | ranges::view::all, 0.);
}


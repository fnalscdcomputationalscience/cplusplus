#include <vector>

using std::vector;

double range_for_loop(vector<double> const& v) {
  double result = 0.;
  for (auto x : v) result += x;
  return result;
}

#include <initializer_list>
#include <type_traits>
#include <iostream>

int main() {
  std::initializer_list<int> lst{ 1, 2, 3};
  std::initializer_list<int> cpy(lst);
  std::cout << std::is_copy_constructible<decltype(cpy)>::value;
}

#include "toarray.hh"

double g(double* x) {
  std::array<double, 5> y;
  for (auto i = 0ULL; i != 5; ++i) y[i] = x[i];
  return std::accumulate(begin(y), end(y), 0.0);
}


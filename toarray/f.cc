#include "toarray.hh"

double f(double* x) {
  std::array<double, 5> y = to_array<5>(x);
  return std::accumulate(begin(y), end(y), 0.0);
}


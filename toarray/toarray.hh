#include <array>
#include <algorithm>
#include <numeric>

template <unsigned long long N>
std::array<double, N> to_array(double const* x) {
  std::array<double, N> res;
  for (unsigned long long i = 0; i < N; ++i) res[i] = x[i];
  return res;
}


#include "toarray.hh"

double h(double* x) {
  std::array<double, 5> y;
  std::copy(x, x+5, y.begin());
  return std::accumulate(begin(y), end(y), 0.0);
}


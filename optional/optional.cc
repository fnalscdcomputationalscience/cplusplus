#include <optional>

class Fcn {
  public:
    explicit  Fcn(double a) : a_(a) { }

  double operator()(double x) const { return a_ * x; }
  private:
  double a_;
};

double f1(double x) {
  std::optional<Fcn> opt;
  opt = Fcn(3.0);

  return (*opt)(x);
}

double f2(double x, std::optional<Fcn> o) {
  return (*o)(x);
}

int xxxxmain() {
  double x = f1(3.0);
  double y = f2(3.0, std::optional<Fcn>(2.0));
  return x+y;
}

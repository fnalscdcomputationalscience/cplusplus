Integer addition and loops {#_integer_addition_and_loops}
==========================

We commonly see the type `int` used in a `for` loop, even when the loop
counter can never be negative. Some people do this out of habit (maybe
picked up from earlier C programming days). Some people do this because
they believe it is more efficient to use a 32-bit `int` (common on
64-bit systems) rather than a 64-bit `unsigned long` (also common on
64-bit systems) as a loop counter. Is this true? Is incrementing `int`
values any more (or less) efficient than adding `unsigned long` values?

The code we will compare is two instantiations of the function template
`increment`:

**increment.hh.**

``` {.c++}
template <class T> T increment(T i) { return ++i; }
```

It turns our that for the x86 hardware, many compilers (here, we are
using GCC 8 on Linux) know that a quick way to increment an integer is
to use the `lea` (load effective address) function. For both 32- and
64-bit integers, this is what our compiler does. The only difference in
the generated code is whether we set the 32-bit register `eax`, or the
64-bit register `rax`. The instruction has a 1-cycle latency in either
case.

What does the disassembled code look like?

**add.int.s.**

``` {.asm}
   0: 8d 47 01                lea    eax,[rdi+0x1]
   3:   c3                      ret
```

**add.unsignedlong.s.**

``` {.asm}
   0: 48 8d 47 01             lea    rax,[rdi+0x1]
   4:   c3                      ret
```

So: the 32-bit code is one byte smaller, but will execute with the same
latency. But what happens if we use the 32-bit integer in a place where
a 64-bit unsigned integer is expected (such as in `vector`
subscripting)? Let's take a look.

**loop.int.**

``` {.c++}
#include <vector>
using vec = std::vector<double>;

double sum(vec const& v) {
  double result { 0.0 };
  for (int i = 0; i < v.size(); ++i) result += v[i];
  return result;
}
```

**loop.unsigned.**

``` {.c++}
#include <vector>
using vec = std::vector<double>;

double sum(vec const& v) {
  double result { 0.0 };
  for (vec::size_type i = 0; i < v.size(); ++i) result += v[i];
  return result;
}
```

**loop.int.s.**

``` {.asm}
   0: 48 8b 17                mov    rdx,QWORD PTR [rdi]
   3:   48 8b 47 08             mov    rax,QWORD PTR [rdi+0x8]
   7:   48 29 d0                sub    rax,rdx
   a:   48 89 c6                mov    rsi,rax
   d:   48 c1 fe 03             sar    rsi,0x3
  11:   74 55                   je     68 <sum(std::vector<double, std::allocator<double> > const&)+0x68>
  13:   48 83 f8 08             cmp    rax,0x8
  17:   74 54                   je     6d <sum(std::vector<double, std::allocator<double> > const&)+0x6d>
  19:   48 89 f1                mov    rcx,rsi
  1c:   48 89 d0                mov    rax,rdx
  1f:   66 0f ef c0             pxor   xmm0,xmm0
  23:   48 d1 e9                shr    rcx,1
  26:   48 c1 e1 04             shl    rcx,0x4
  2a:   48 01 d1                add    rcx,rdx
  2d:   0f 1f 00                nop    DWORD PTR [rax]
  30:   f2 0f 10 08             movsd  xmm1,QWORD PTR [rax]
  34:   48 83 c0 10             add    rax,0x10
  38:   f2 0f 58 c1             addsd  xmm0,xmm1
  3c:   f2 0f 10 48 f8          movsd  xmm1,QWORD PTR [rax-0x8]
  41:   f2 0f 58 c1             addsd  xmm0,xmm1
  45:   48 39 c8                cmp    rax,rcx
  48:   75 e6                   jne    30 <sum(std::vector<double, std::allocator<double> > const&)+0x30>
  4a:   48 89 f0                mov    rax,rsi
  4d:   48 83 e0 fe             and    rax,0xfffffffffffffffe
  51:   48 39 c6                cmp    rsi,rax
  54:   74 0a                   je     60 <sum(std::vector<double, std::allocator<double> > const&)+0x60>
  56:   f2 0f 58 04 c2          addsd  xmm0,QWORD PTR [rdx+rax*8]
  5b:   c3                      ret
  5c:   0f 1f 40 00             nop    DWORD PTR [rax+0x0]
  60:   c3                      ret
  61:   0f 1f 80 00 00 00 00    nop    DWORD PTR [rax+0x0]
  68:   66 0f ef c0             pxor   xmm0,xmm0
  6c:   c3                      ret
  6d:   66 0f ef c0             pxor   xmm0,xmm0
  71:   31 c0                   xor    eax,eax
  73:   eb e1                   jmp    56 <sum(std::vector<double, std::allocator<double> > const&)+0x56>
```

**loop.unsignedlong.s.**

``` {.asm}
   0: 48 8b 17                mov    rdx,QWORD PTR [rdi]
   3:   48 8b 47 08             mov    rax,QWORD PTR [rdi+0x8]
   7:   48 29 d0                sub    rax,rdx
   a:   48 89 c6                mov    rsi,rax
   d:   48 c1 fe 03             sar    rsi,0x3
  11:   74 55                   je     68 <sum(std::vector<double, std::allocator<double> > const&)+0x68>
  13:   48 83 f8 08             cmp    rax,0x8
  17:   74 54                   je     6d <sum(std::vector<double, std::allocator<double> > const&)+0x6d>
  19:   48 89 f1                mov    rcx,rsi
  1c:   48 89 d0                mov    rax,rdx
  1f:   66 0f ef c0             pxor   xmm0,xmm0
  23:   48 d1 e9                shr    rcx,1
  26:   48 c1 e1 04             shl    rcx,0x4
  2a:   48 01 d1                add    rcx,rdx
  2d:   0f 1f 00                nop    DWORD PTR [rax]
  30:   f2 0f 10 08             movsd  xmm1,QWORD PTR [rax]
  34:   48 83 c0 10             add    rax,0x10
  38:   f2 0f 58 c1             addsd  xmm0,xmm1
  3c:   f2 0f 10 48 f8          movsd  xmm1,QWORD PTR [rax-0x8]
  41:   f2 0f 58 c1             addsd  xmm0,xmm1
  45:   48 39 c1                cmp    rcx,rax
  48:   75 e6                   jne    30 <sum(std::vector<double, std::allocator<double> > const&)+0x30>
  4a:   48 89 f0                mov    rax,rsi
  4d:   48 83 e0 fe             and    rax,0xfffffffffffffffe
  51:   48 39 f0                cmp    rax,rsi
  54:   74 0a                   je     60 <sum(std::vector<double, std::allocator<double> > const&)+0x60>
  56:   f2 0f 58 04 c2          addsd  xmm0,QWORD PTR [rdx+rax*8]
  5b:   c3                      ret
  5c:   0f 1f 40 00             nop    DWORD PTR [rax+0x0]
  60:   c3                      ret
  61:   0f 1f 80 00 00 00 00    nop    DWORD PTR [rax+0x0]
  68:   66 0f ef c0             pxor   xmm0,xmm0
  6c:   c3                      ret
  6d:   31 c0                   xor    eax,eax
  6f:   66 0f ef c0             pxor   xmm0,xmm0
  73:   eb e1                   jmp    56 <sum(std::vector<double, std::allocator<double> > const&)+0x56>
```

The assembly code very similar. Here are the details:

The code using `int` is identical to the code using `size_t`, using GCC
7.3. However, if we use GCC 6.3, there is a small difference: the code
using `int` is very slightly smaller (48 bytes, versus 52 bytes). If we
use the native clang compiler on Yosemite, we end up with identical
assembly code between the two cases. The exact results are
implementation-dependent. Why is this?

Formally, the code using `int` invokes **undefined behavior**. Undefined
behavior does not mean something disastrous will happen. In this case,
nothing does. What it means is that the implementation is free to do
something clever, but with the caveat that the clever thing might not
always work. In this case, the thing that might fail only happens if the
`vector` is too long for its size to be correctly represented as an
`int`. That's rather unlikely to occur in a real program (a
`vector<double>` of such a size would occupy about 16 GB of memory). The
code using the 64-bit unsigned integer does not invoke undefined
behavior.

So, there is little difference resulting from the use the 32-bit `int`
rather than the 64-bit `unsigned` type (and, for some compilers, no
difference).

Finally, consider the **range for** version of the code:

**loop.range.**

``` {.c++}
#include <vector>
using vec = std::vector<double>;

double sum(vec const& v) {
  double result { 0.0 };
  for (auto x : v)
    result += x;
  return result;
}
```

This yields:

**loop.range.asm.**

``` {.asm}
   0: 48 8b 17                mov    rdx,QWORD PTR [rdi]
   3:   48 8b 4f 08             mov    rcx,QWORD PTR [rdi+0x8]
   7:   48 39 ca                cmp    rdx,rcx
   a:   74 6c                   je     78 <sum(std::vector<double, std::allocator<double> > const&)+0x78>
   c:   48 83 e9 08             sub    rcx,0x8
  10:   48 89 d0                mov    rax,rdx
  13:   48 89 ce                mov    rsi,rcx
  16:   48 29 d6                sub    rsi,rdx
  19:   48 c1 ee 03             shr    rsi,0x3
  1d:   48 83 c6 01             add    rsi,0x1
  21:   48 39 ca                cmp    rdx,rcx
  24:   74 57                   je     7d <sum(std::vector<double, std::allocator<double> > const&)+0x7d>
  26:   48 89 f1                mov    rcx,rsi
  29:   66 0f ef c0             pxor   xmm0,xmm0
  2d:   48 d1 e9                shr    rcx,1
  30:   48 c1 e1 04             shl    rcx,0x4
  34:   48 01 d1                add    rcx,rdx
  37:   66 0f 1f 84 00 00 00    nop    WORD PTR [rax+rax*1+0x0]
  3e:   00 00
  40:   f2 0f 10 08             movsd  xmm1,QWORD PTR [rax]
  44:   48 83 c0 10             add    rax,0x10
  48:   f2 0f 58 c1             addsd  xmm0,xmm1
  4c:   f2 0f 10 48 f8          movsd  xmm1,QWORD PTR [rax-0x8]
  51:   f2 0f 58 c1             addsd  xmm0,xmm1
  55:   48 39 c8                cmp    rax,rcx
  58:   75 e6                   jne    40 <sum(std::vector<double, std::allocator<double> > const&)+0x40>
  5a:   48 89 f0                mov    rax,rsi
  5d:   48 83 e0 fe             and    rax,0xfffffffffffffffe
  61:   48 8d 14 c2             lea    rdx,[rdx+rax*8]
  65:   48 39 c6                cmp    rsi,rax
  68:   74 06                   je     70 <sum(std::vector<double, std::allocator<double> > const&)+0x70>
  6a:   f2 0f 58 02             addsd  xmm0,QWORD PTR [rdx]
  6e:   c3                      ret
  6f:   90                      nop
  70:   c3                      ret
  71:   0f 1f 80 00 00 00 00    nop    DWORD PTR [rax+0x0]
  78:   66 0f ef c0             pxor   xmm0,xmm0
  7c:   c3                      ret
  7d:   66 0f ef c0             pxor   xmm0,xmm0
  81:   eb e7                   jmp    6a <sum(std::vector<double, std::allocator<double> > const&)+0x6a>
```

This is the smallest (32 bytes), and likely the fastest, version of the
code. This demonstrates that newer and more convenient syntax can
actually yield the most efficient code! In short, when a **range for**
loop will meet your needs, it is the preferred way of writing a for
loop:

-   the loop is clear,

-   the loop is succinct, and

-   the loop is efficient.

That's as optimal such code can be.

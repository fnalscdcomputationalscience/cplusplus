#include <vector>
using vec = std::vector<double>;

double sum(vec const& v) {
  double result { 0.0 };
  for (vec::size_type i = 0; i < v.size(); ++i) result += v[i];
  return result;
}

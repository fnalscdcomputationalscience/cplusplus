#include <vector>
#include <numeric>
using vec = std::vector<double>;

double sum(vec const& v) {
  return std::accumulate(begin(v), end(v), 0.0);
}

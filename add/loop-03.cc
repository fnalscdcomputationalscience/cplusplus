#include <vector>
using vec = std::vector<double>;

double sum(vec const& v) {
  double result { 0.0 };
  for (auto x : v)
    result += x;
  return result;
}

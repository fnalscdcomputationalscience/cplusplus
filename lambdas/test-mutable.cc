#include <algorithm>
#include <vector>
#include <string>
#include <iostream>

int main() {
  int count = 0;
  std::vector<std::string> words{"An", "ancient", "pond"};
  std::for_each(words.cbegin(), words.cend(),
                [&count](const std::string &word) {
                  if (isupper(word[0])) {
                    std::cout << word << " ";
                    count++;
                  }
                });
  std::cout << std::endl;
  std::cout << "Count is: " << count << std::endl;
}

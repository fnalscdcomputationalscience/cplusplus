#include <iostream>

struct X
{
  X() { std::cout << "Created an X at: " << this << '\n'; }
};

#include "tuple_utility.hpp"

#include <tuple>
#include <vector>
#include <iostream>
#include <utility>

template <class F, size_t... Is>
constexpr auto index_apply_impl(F f,
                                std::index_sequence<Is...>) {
    return f(Is...);
}

template <size_t N, class F>
constexpr auto index_apply(F f) {
    return index_apply_impl(f, std::index_sequence_for<N>{});
}

template <typename... Ts>
struct tuplevec {
  std::tuple<std::vector<Ts>...> data;

  void insert(Ts... vals) {
    index_apply<sizeof...(vals)>([this](auto... Is) {
      get<Is>(this->data).insert(get<Is>(vals));
    })
    tuple_for_each([vals...](auto& v){v.push_back(vals...);});
  }
  
  std::size_t size() const { return std::get<0>(data).size(); }
  
  explicit tuplevec(size_t n) {
    tuple_for_each([n](auto& v){v.reserve(n);}, data);
  }
};


int main() {
  tuplevec<double, int> tv(10);
  std::cout << std::get<1>(tv.data).capacity() << std::endl;
  tv.insert(2.25,7);
  tv.insert(1.5, -1);
  std::cout << tv.size() << std::endl;
  return 0;
}

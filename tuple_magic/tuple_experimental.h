#ifndef TUPLE_EXPERIMENTAL_H
#define TUPLE_EXPERIMENTAL_H

#include <tuple>
#include <utility>

namespace std {
  namespace experimental {

    template <typename F, typename Tuple, size_t... I>
    constexpr decltype(auto) apply_impl(F&& f, Tuple&& t, std::index_sequence<I...>) {
      // Do pack expansion of I... over the whole expression
      return std::forward<F>(f)(std::get<I>(std::forward<Tuple>(t))...);
    }

    template <typename F, typename Tuple>
    constexpr decltype(auto) apply(F&& f, Tuple&& t) {
      using Indices = std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>;
      return apply_impl(std::forward<F>(f), std::forward<Tuple>(t), Indices {});
    }
  } // namespace experimental
} // namespace std

#endif

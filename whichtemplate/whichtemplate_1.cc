template <typename T> int f(T)    { return 0; }
template <>           int f(int*) { return 1; }
template <typename T> int f(T*)   { return 2; }
template <>           int f(int*) { return 3; }

int main()
{
  int* p = nullptr;
  return f(p);
}


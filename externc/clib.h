#ifndef CLIB_H
#define CLIB_H
#if defined(__cplusplus)
extern "C"
#endif

void cfunc(void(*pf)(char const*), const char* msg);

#endif

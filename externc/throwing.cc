#include <stdexcept>
#include "throwing.hh"

void throwing(char const* msg) { throw std::runtime_error(msg); }

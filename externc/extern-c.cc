#include "clib.h"
#include "throwing.hh"
#include <iostream>

int
main()
{
  try {
    cfunc(&throwing, "moo");
    std::cout << "Boo! Failed to throw!\n";
  }
  catch (std::exception const& ex) {
    std::cout << "Hooray! We caught an exception!\n";
  }
  catch (...) {
    std::cout << "Boo! Caught wrong thing!\n";
  }
  std::cout << "We're outta here!\n";
}

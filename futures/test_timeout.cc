#include <thread>
#include <future>
#include <chrono>
#include <iostream>
#include <atomic> 

struct X
{
  ~X() { std::cout << "Destroyed X at: " << this << '\n'; }
};

int loopy(std::atomic_bool& run)
{
  X x;
  int i = 0;
  while(run) {++i;}
  return i;
}

int main()
{
  std::atomic_bool run = true;
  std::future<int> result = std::async(std::launch::async, loopy, std::ref(run));
  std::future_status status = result.wait_for(std::chrono::seconds(1));

  if (status == std::future_status::deferred)
    std::cout << "work did not start\n";
  else if (status == std::future_status::ready) {
    std::cout << "calculation completed\n";
    std::cout << "result is: " << result.get() << '\n';
  }
  else if (status == std::future_status::timeout) {
    std::cout << "calculation timed out\n";
    run = false;
  }
  else
    std::cout << "Unknown value returned from wait_for\n";
  return 0;
}

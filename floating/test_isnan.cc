#include <cmath>

bool tester(double val)
{
  return std::isnan(val);
}
// This is a program to find the largest integer that can be exactly
// represented as an IEEE float, and similar for IEEE double.

#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits>

using std::nextafter;

template <typename T>
void
find_maximum_integral()
{
  using nl = std::numeric_limits<T>;
  // Start at our guess, based on the number of mantissa bits.
  unsigned long long start = (1ULL << nl::digits) - 10;
  std::cout << "We are starting at " << start << std::endl; // we want the flush
  T x = static_cast<T>(start);
  T next = x + 1.0f;
  while (x != next) {
    x = next;
    next = x + 1.0f;
  }

  std::cout << "Biggest integer exactly represented is: "
            << std::setprecision(nl::digits10 + 1) << std::fixed << next
            << std::endl; // we really want the flush
}

int
main()
{
  find_maximum_integral<float>();
  find_maximum_integral<double>();
}

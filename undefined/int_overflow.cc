// unsigned int overflow is well-defined: wrap-around.
// (signed) int overflow is undefined behavior. So the compiler is free
// to assume it never happens.

bool f(int x) {
  return x+1 > x;
}

bool g(unsigned x) {
  return x+1 > x;
}

int main(int argc, char* argv[]) {

  return f(argc) + g(argc);
}

#include <cassert>
#include <functional>

int main()
{
  std::size_t s1 {393444ULL};
  std::size_t h1 { std::hash<std::size_t>()(s1) };
  assert(s1 == h1);
}

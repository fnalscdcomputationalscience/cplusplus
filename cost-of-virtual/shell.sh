#!/usr/bin/env bash
IMAGE=gcc:6.3.0
docker run --privileged --security-opt seccomp=unconfined --rm -it -v $PWD:/work -w /work $(IMAGE)

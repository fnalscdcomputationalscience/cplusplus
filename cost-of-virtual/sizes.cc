#include "energy.h"
#include <iostream>
#include <memory>
#include <typeinfo>
using namespace std;

template <class T> void printsize() {
cout << "size of " << typeid(T).name() << " = " << sizeof(T) << " bytes\n";
}

int main() {
  printsize<float>();
  printsize<SimpleEnergy>();
  printsize<Energy>();
  printsize<EnergyWithError>();
}

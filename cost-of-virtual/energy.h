#ifndef ENERGY_H
#define ENERGY_H

class SimpleEnergy {
  float val_ = 0.0;
public:
  explicit SimpleEnergy(float e) : val_(e) {}
  float val() const { return val_; }
};

class Energy {
  float val_ = 0.0;
public:
  explicit Energy(float e) : val_(e) {}
  virtual ~Energy() = default;
  virtual float val() const { return val_; }
};

class EnergyWithError : public Energy {
  float err_ = 0.0;
public:
   EnergyWithError(float e, float err) : Energy(e), err_(err) { }
   
  virtual float err() const { return err_; }
};

#endif

#include "energy.h"
float get_energy(SimpleEnergy e)        { return e.val(); } // #1
float get_energy(SimpleEnergy const& e) { return e.val(); } // #2
float get_energy(Energy e)              { return e.val(); } // #3
float get_energy(Energy const& e)       { return e.val(); } // #4

#include "energy.h"

float get_energy(Energy const&);

int main() 
{
   Energy e(15.);
   EnergyWithError ewe(255., 1.);
   float f = get_energy(e);
   float g = get_energy(ewe);
   return static_cast<int>(f)+static_cast<int>(g);
}

#include "benchmark/benchmark.h"
#include "range/v3/all.hpp"

#include <algorithm>
#include <cassert>
#include <random>
#include <vector>
using std::vector;
using namespace ranges::v3;

// Generate a vector<double> of length 'n'. The vector is filled with uniform
// random numbers, chosen so that about half will pass the selection criteria
// used below.
std::vector<double>
mygen(std::size_t n)
{
  std::default_random_engine generator(7);
  std::vector<double> res(n);
  std::uniform_int_distribution<int> flat(1, 6);
  for (auto& v : res)
    v = flat(generator);
  return res;
}

// g1 does three operations:
//   1. multiply input by factor
//   2. select values smaller than threshold
//   3. sum the resulting values
double
g1(std::vector<double> const& vals)
{
  auto multiply_by_factor = [](auto x) { return 2.5 * x; };
  auto is_small = [](auto x) { return x < 5.0; };
  return ranges::accumulate(
    vals | view::transform(multiply_by_factor) | view::filter(is_small), 0.0);
}

// g2 is an optimized g1:
//   1. select values smaller than a reduced threshold
//   2. sum the resulting values
//   3. scale the sum by the multiplicative factor
double
g2(std::vector<double> const& vals)
{
  auto is_small = [](auto x) { return x < 2.0; };
  return 2.5 * accumulate(vals | view::filter(is_small), 0.0);
}

// f1 uses an explicit loop:
//   1. multiply each number
//   2. if the result is lower than the threshold, add it to the running sum.
double
f1(std::vector<double> const& vals)
{
  double res = 0.0;
  for (auto v : vals) {
    double tmp = 2.5 * v;
    if (tmp < 5.0)
      res += tmp;
  }
  return res;
}

// f2 contains a reduction in strength on f1
//   1. test each number against the reduced threshold
//   2. only those that pass are multiplied and summed
double
f2(std::vector<double> const& vals)
{
  double res = 0.0;
  for (auto v : vals) {
    if (v < 2.0)
      res += 2.5 * v;
  }
  return res;
}

// f3 is like f2, but uses a ternary operator rather than an if.
double
f3(std::vector<double> const& vals)
{
  double res = 0.0;
  for (auto v : vals) {
    res += (v < 2.0) ? 2.5 * v : 0.0;
  }
  return res;
}

// f4 is like f2, but does the sum of unmultiplied values,
// and performs just one multiplication on the result.
double
f4(std::vector<double> const& vals)
{
  double res = 0.0;
  for (auto v : vals) {
    if (v < 2.0) {
      res += v;
    }
  }
  return 2.5 * res;
}

// f5 is like f3 (using the ternary operator),
// but summs unmultiplied values and does one
// multiplication on the sum.
double
f5(std::vector<double> const& vals)
{
  double res = 0.0;
  for (auto v : vals) {
    res += (v < 2.0) ? v : 0.0;
  }
  return res * 2.5;
}

// f6 does:
//   1. partition the input, so that small values are first.
//   2. sum the small values
//   3. multiply the sum only once.
double
f6(std::vector<double> vals)
{
  auto is_small = [](auto x) { return x < 2.0; };
  auto e = std::partition(vals.begin(), vals.end(), is_small);
  return 2.5 * std::accumulate(vals.begin(), e, 0.0);
}

static void
BM_g1(benchmark::State& state)
{
  auto vec = mygen(state.range(0));
  for (auto _ : state) {
    double x = g1(vec);
    benchmark::DoNotOptimize(x);
  }
}

static void
BM_g2(benchmark::State& state)
{
  auto vec = mygen(state.range(0));
  for (auto _ : state) {
    double x = g2(vec);
    benchmark::DoNotOptimize(x);
  }
}

static void
BM_f1(benchmark::State& state)
{
  auto vec = mygen(state.range(0));
  for (auto _ : state) {
    double x = f1(vec);
    benchmark::DoNotOptimize(x);
  }
}

static void
BM_f2(benchmark::State& state)
{
  auto vec = mygen(state.range(0));
  for (auto _ : state) {
    double x = f2(vec);
    benchmark::DoNotOptimize(x);
  }
}

static void
BM_f3(benchmark::State& state)
{
  auto vec = mygen(state.range(0));
  for (auto _ : state) {
    double x = f3(vec);
    benchmark::DoNotOptimize(x);
  }
}

static void
BM_f4(benchmark::State& state)
{
  auto vec = mygen(state.range(0));
  for (auto _ : state) {
    double x = f4(vec);
    benchmark::DoNotOptimize(x);
  }
}

static void
BM_f5(benchmark::State& state)
{
  auto vec = mygen(state.range(0));
  for (auto _ : state) {
    double x = f5(vec);
    benchmark::DoNotOptimize(x);
  }
}

static void
BM_f6(benchmark::State& state)
{
  auto vec = mygen(state.range(0));
  for (auto _ : state) {
    double x = f6(vec);
    benchmark::DoNotOptimize(x);
  }
}

BENCHMARK(BM_g1)->Range(8, 8<<20);
BENCHMARK(BM_g2)->Range(8, 8<<20);
BENCHMARK(BM_f1)->Range(8, 8<<20);
BENCHMARK(BM_f2)->Range(8, 8<<20);
BENCHMARK(BM_f3)->Range(8, 8<<20);
BENCHMARK(BM_f4)->Range(8, 8<<20);
BENCHMARK(BM_f5)->Range(8, 8<<20);
BENCHMARK(BM_f6)->Range(8, 8<<20);

BENCHMARK_MAIN();

// Comment out BENCHMARK_MAIN (above), and uncomment the following,
// to test that the implementations all return the same values.

// int main() {
//    std::vector<double> x {1.5, 2.5, 3.5, 10., 12.};
//    auto a = g1(x);
//    assert(a==g2(x));
//    assert(a == f1(x));
//    assert(a == f2(x));
//    assert(a == f3(x));
//    assert(a == f4(x));
//    assert(a == f5(x));
//    assert(a == f6(x));
// }

#include <type_traits>
#include <iostream>
#include <typeinfo>

// Introduce a new concept named Float
template <typename T>
concept bool Float = std::is_floating_point<T>::value;

template<Float T, Float U>
auto
mymin(T a, U b)
{
  return b > a ? a : b;
}

int
main()
{
  double const x = 13.0;
  float const y = 10.0;
  auto const z = mymin(x, y);
  // The following line will not compile. Uncomment it to see the
  // error message.
  //auto const u = mymin(x,"cow");
  std::cout << "type of x is: " << typeid(x).name() << std::endl;
  std::cout << "type of y is: " << typeid(y).name() << std::endl;
  std::cout << "type of z is: " << typeid(z).name() << std::endl;
  //std::cout << u << std::endl;
}

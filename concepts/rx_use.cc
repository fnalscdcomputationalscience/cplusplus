#include "benchmark/benchmark.h"

#include "rx/ranges.hpp"

#include <algorithm>
#include <cassert>
#include <random>
#include <vector>
using std::vector;
using namespace rx;

// Generate a vector<double> of length 'n'. The vector is filled with uniform
// random numbers, chosen so that about half will pass the selection criteria
// used below.
std::vector<double>
mygen(std::size_t n)
{
  std::default_random_engine generator(7);
  std::vector<double> res(n);
  std::uniform_int_distribution<int> flat(1, 6);
  for (auto& v : res)
    v = flat(generator);
  return res;
}

// r1 does three operations:
//   1. multiply input by factor
//   2. select values smaller than threshold
//   3. sum the resulting values
double
r1(std::vector<double> const& vals)
{
  auto multiply_by_factor = [](auto x) { return 2.5 * x; };
  auto is_small = [](auto x) { return x < 5.0; };
  return
    vals | transform(multiply_by_factor) | filter(is_small) | sum();
}

// r2 is an optimized r1:
//   1. select values smaller than a reduced threshold
//   2. sum the resulting values
//   3. scale the sum by the multiplicative factor
double
r2(std::vector<double> const& vals)
{
  auto is_small = [](auto x) { return x < 2.0; };
  return 2.5 * (vals | rx::filter(is_small) | sum());
}

static void
BM_r1(benchmark::State& state)
{
  auto vec = mygen(state.range(0));
  for (auto _ : state) {
    double x = r1(vec);
    benchmark::DoNotOptimize(x);
  }
}

static void
BM_r2(benchmark::State& state)
{
  auto vec = mygen(state.range(0));
  for (auto _ : state) {
    double x = r2(vec);
    benchmark::DoNotOptimize(x);
  }
}

BENCHMARK(BM_r1)->Range(8, 8<<20);
BENCHMARK(BM_r2)->Range(8, 8<<20);

BENCHMARK_MAIN();

// Comment out BENCHMARK_MAIN (above), and uncomment the following,
// to test that the implementations all return the same values.

// int main() {
//    std::vector<double> x {1.5, 2.5, 3.5, 10., 12.};
//    auto a = g1(x);
//    assert(a==g2(x));
//    assert(a == f1(x));
//    assert(a == f2(x));
//    assert(a == f3(x));
//    assert(a == f4(x));
//    assert(a == f5(x));
//    assert(a == f6(x));
// }

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include "range/v3/all.hpp"

class person_t
{
public:
  person_t(std::string n, std::string sn, bool male) noexcept
    : m_name(std::move(n))
    , m_surname(std::move(sn))
    , m_male(male)
  {}

  person_t(person_t const& p)
    : m_name(p.m_name)
    , m_surname(p.m_surname)
    , m_male(p.m_male)
  {
    std::cout << "copy-constructed person_t at " << this << '\n';
  }

  std::string name() const { return m_name; }

  std::string surname() const { return m_surname; }

  bool is_female() const { return !m_male; }

private:
  std::string m_name;
  std::string m_surname;
  bool m_male;
};

bool
is_female(person_t const& p)
{
  return p.is_female();
}
// The following function is NOT the kind of predicate we want to use.
// We have it here to demonstrate error messages. The horror! The horror!
bool is_male(std::string)
{
  return false;
}

std::string
name(person_t const& p)
{
  return p.name();
}

int
main()
{
  using std::string;
  using std::vector;
  using namespace ranges::v3;

  vector<person_t> const people{ { "Moe", "Smith", true },
                                 { "Sally", "Jones", false },
                                 { "Roberta", "Roe", false },
                                 { "Bob", "Bubba", true } };
  std::cout << "Done constructing the vector<person_t>\n";
  vector<string> const women_names =
    people | view::filter(is_female) | view::transform(name);
  for (auto const& n : women_names) {
    std::cout << n << '\n';
  }

  // Uncomment the following to see the compilation error.
  // vector<string> const n_names =
  //  people | view::filter(is_male) | view::transform(name);
}

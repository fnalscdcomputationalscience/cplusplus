---
title: Fitting Histograms
---

A histogram is a tool for density estimation of a continuous variable
(or variabes), invented by Karl Pearson. To construct a histogram, one
first *bins* the range of the variable or variables, and then *counts*
the number of entries in each bin. Thus the statistics of a histogram
follow a statistical distribution that predicts an integer number of
counts per bin.

In particle physics, parameter estimation ("fitting") problems that
invovle histograms typically involve one of two different types of
mathematical models. In one case, the model in question describes the
*shape* of the distribution of observed data, but not the *number* of
observed values. This is the case, for example, when a fixed number of
prepared observations are used to estimate a selection efficiency. In
the other case, the model predicts both the shape of the distribution
*and* the number of observed data; this is the common case for measured
differential cross sections.

If the model include specification of the number of observations, then
the probability distribution that describes the likelihood of observing
a given number of counts in each bin is the *multinomial distribution*.
If the number of observations is not a parameter of the model, but
rather is a prediction of the model, then whatever probability
distribution describes the likelihood of obtaining a given number of
observations determines the probability of observing the data in each
bin independently, and the probability of observing the ensemble of bin
is the product of the individual probabilities. In the mesaurement of
differential cross sections, this is typically the *Poisson*
distribution.

$f(k) = {n \choose k} p^{k} (1-p)^{n-k}$

What did that do?

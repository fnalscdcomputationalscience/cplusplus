#include <vector>

inline bool f(std::size_t num, double val, std::vector<double>& out) {
  if (val < 0.) return false;
  out.resize(num);
  for (std::size_t i = 0; i != num; ++i) {
    out[i] = i * val;
  }
  return true;
}

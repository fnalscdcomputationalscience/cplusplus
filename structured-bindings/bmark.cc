#include <benchmark/benchmark.h>
// Note the weird style: the *.cc files are really headers, but are
// named *.cc files in order to use them in default make rules; we
// want to make sure they compile stand-alone.
#include "old_return.cc"
#include "new_return.cc"
#include "without_move.cc"
#include "return_slot.cc"

static void BM_old_return(benchmark::State& state) {
  for (auto _ : state) {
    std::vector<double> x;
    bool res = f(100, 2.5, x);
    benchmark::DoNotOptimize(x);
    benchmark::DoNotOptimize(res);
  }
}
// Register the function as a benchmark
BENCHMARK(BM_old_return);

// Define another benchmark
static void BM_new_return(benchmark::State& state) {
  for (auto _ : state) {
    auto [status, res] = g(100, 2.5);
    benchmark::DoNotOptimize(status);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_new_return);

static void BM_without_move(benchmark::State& state) {
  for (auto _ : state) {
    auto [status, res] = h(100, 2.5);
    benchmark::DoNotOptimize(status);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_without_move);

static void BM_return_slot(benchmark::State& state) {
  for (auto _ : state) {
    auto [status, res] = k(100, 2.5);
    benchmark::DoNotOptimize(status);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_return_slot);

BENCHMARK_MAIN();

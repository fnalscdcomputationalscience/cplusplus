
== Structured bindings

The C++17 standard introduces a facility that can automatically
"unpack" a variety of data types into their constituent elements.
Included among the types that can be so unpacked is std::pair<T,U>,
which is the `value_type` of `std::map` (and of the newer
`std::unordered_map`).

Consider how, in C++14, we would iterate through a `map` to
accumulate the stored values (here we don't care about the keys).
The code to do this looks like:

[source,c++]
.old_map.cc
----
include::./old_map.cc[lines=1..-1]
----

In C++17, we have a better option:

[source,c++]
.sb_map.cc
----
include::./sb_map.cc[lines=1..-1]
----

These two functions yield identical assembly language when
compiled, so there is no performance difference. The only
difference is the notional convenience of the structured
binding.

Structured bindings can also be used to unpack `std::tuple`
instances, making it feasible for C++ functions to return
multiple objects, and removing the need for "output arguments".
In combination with move semantics (added to C++ in the C++11),
and with guaranteed copy elision (since C++11), returning
multiple values is more convenient, and maybe more efficient,
than using output arguments.

Consider a simple case: a function that is to return a
`std::vector` of objects, and a `bool` status that informs
the caller of whether the output is "good" (presumably
because an empty output `vector` might be good, or it
might be bad). Prior to C++17, the common way to write
such a function would be to take the `vector` by non-const
reference, indicating an output argument, and to return
the  `bool`. Function documentation would have to say
whether the initial state of the `vector` was expected
to be empty, or perhaps that it would be cleared by
the function, or perhaps the function would always
extend the `vector`, leaving any original values in
place. Common choices would lead to code like:

[source,c++]
.old_return.cc
----
include::./old_return.cc[lines=1..-1]
----

In C++17, this can be written in a more transparent
manner. This code makes it clear that the resulting
`vector` contains only the elements returned from
the called function, because there is no input 
`vector`:

[source,c++]
.new_return.cc
----
include::./new_return.cc[lines=1..-1]
----

A third version, with the function `h` that differs from `g`
only in that it does not force use of the move constructor
in forming the return value, shows the additional cost incurred
by making a copy.

[source,c++]
.without_move.cc
----
include::./without_move.cc[lines=1..-1]
----

Our final version has the function `k` written to assure that
the *return slot* is used for the object `result`. This means
it is returned without a copy, not even a move-copy.

[source,c++]
.return_slot.cc
----
include::./return_slot.cc[lines=1..-1]
----

To understand the performance of the various implementation, we measure
them using the Google benchmark library.
The result is that the performance of the first, second and fourth 
implementations are indistinguishable. The third
function is clearly slower.
The benchmarking code is simple:

[source,c++]
.bmark.cc
----
include::./bmark.cc[lines=5..-1]
----


----
include::./bmark.out[lines=1..-1]
----


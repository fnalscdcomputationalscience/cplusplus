#include <vector>
#include <utility> // for std::pair

inline std::pair<bool, std::vector<double>> h(std::size_t num, double val) {
  if (val < 0.0) return { false, {} };
  std::vector<double> res(num);
  for (std::size_t i = 0; i != num; ++i) {
    res[i] = i * val;
  }
  return {true, res};
}

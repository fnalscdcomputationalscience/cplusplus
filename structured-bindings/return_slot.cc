#include <vector>
#include <utility> // for std::pair

inline std::pair<bool, std::vector<double>> k(std::size_t num, double val) {
  if (val < 0.0) return { false, {} };
  std::pair result { true, std::vector<double>(num) };
  std::vector<double>& r = result.second;
  for (std::size_t i = 0; i != num; ++i) {
    r[i] = i * val;
  }
  return result;
}

#include <map>
using LookupTable = std::map<int, double>;
double f(LookupTable const& table) {
  double sum = 0.0;
  for (auto i = table.begin(),
            e = table.end();
       i != e;
       ++i) {
    sum += i->second;
  }
  return sum;
}

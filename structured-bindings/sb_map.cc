#include <map>
using LookupTable = std::map<int, double>;
double f(LookupTable const& table) {
  double sum = 0.0;
  for (auto [ _, v] : table) {
    sum += v;
  }
  return sum;
}

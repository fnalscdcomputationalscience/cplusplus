#include <vector>
#include <utility> // for std::pair and std::move

inline std::pair<bool, std::vector<double>> g(std::size_t num, double val) {
  if (val < 0.0) return { false, {} };
  std::vector<double> res(num);
  for (std::size_t i = 0; i != num; ++i) {
    res[i] = i * val;
  }
  return {true, std::move(res)};
}

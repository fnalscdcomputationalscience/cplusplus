#include <atomic>

int f(int i)
{
  std::atomic<int> j {0};
  j += i;
  return j;
}

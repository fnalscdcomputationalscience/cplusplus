#include "useofbits.hh"

BitField
set_with_bits(uint64_t reading,
              uint64_t count,
              uint64_t flag) {
  BitField a;
  a.reading = reading;
  a.count = count;
  a.flag = flag;
  return a;
}

#include "TH1D.h"
#include "lambda.h"

void fill_hist_3a(std::vector<double> const& nums, TH1D& h) {
  for (auto x : nums) { h.Fill(x); }
}



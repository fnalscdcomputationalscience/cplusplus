#include <thread>
#include <mutex>
std::mutex m1;

double f(double x, double y, double z)
{
  x *= 2.0;
  {
    std::lock_guard<std::mutex> g(m1);
    y *= 3.0;
  }
  z *= 4.0;
  return x+y+z;
}


double g(double x, double y, double z)
{
  x *= 2.0;
  y *= 3.0;
  z *= 4.0;
  return x+y+z;
}

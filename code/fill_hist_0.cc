#include "TH1D.h"
#include "lambda.h"

void fill_hist_0(std::vector<double> const& nums, TH1D& h) {
  for (auto i = 0UL, sz = nums.size(); i!=sz; ++i)
    { h.Fill(nums[i]); }
}



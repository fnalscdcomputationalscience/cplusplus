#include "TH1D.h"
#include "lambda.h"
#pragma GCC diagnostic push ignored "-Wsign-compare"
void fill_hist_0a(std::vector<double> const& nums, TH1D& h) {
  for (int i = 0; i != nums.size(); i++)
    { h.Fill(nums[i]); }
}
#pragma GCC diagnostic pop




#include <vector>
#include <numeric>
//#pragma GCC diagnostic ignored "-Wsign-compare"
double sum_0c(std::vector<double> const& x) {
  double sum = 0.0;
  auto sz = x.size();
  for (auto i = 0UL; i < sz; ++i)
    { sum = sum + x[i]; }
  return sum;
}
#pragma GCC diagnostic pop

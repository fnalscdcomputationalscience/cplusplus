#include <algorithm>

#include "TH1D.h"
#include "lambda.h"

void fill_hist_1(std::vector<double> const& nums, TH1D& h)
{
  for_each(begin(nums),
           end(nums),
           [&h](double x){ h.Fill(x); });
}

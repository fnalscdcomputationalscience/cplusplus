#include <utility>

template <class RT, class... Args>
struct Signal {
  typedef RT (*func_t) (Args ...);
  func_t f; // Stored callback.
  RT invoke(Args... a) const {
    return f(std::forward<Args>(a)...);
  }
};
using OneArgSignal = Signal<int, int>;
using TwoArgSignal = Signal<int, int, long>;

int f1(OneArgSignal s, int a) {
  return s.invoke(a);
}
long f2(TwoArgSignal s, int a, long b) {
  return s.invoke(a, b);
}

#include <iostream>
#include <cmath>

using std::cout;
int main()
{
  OneArgSignal s1 { [](int x) { return 3+x; } };
  TwoArgSignal s2 { [](int x, long y) -> int { return x*y; } };
  cout << f1(s1, 7) << "\n";
  cout << f2(s2, 3, 4) << "\n";
}

#include <cmath>
#include <iomanip>
#include <iostream>

// All constexpr functions are evaluated at compile-time.
namespace nm {
  struct Constants {
    // Note that one can call atan() directly here with constant
    // arguments.
    static double mpi() { return 4.0 * std::atan(1.0); }
    static double h() { return 6.62606957e-34; }
    static double hbar() { return h() / (2.0 * mpi()); }
    };
  }

int main() {
  using nm::Constants;
  std::cout << std::setprecision(12);
  std::cout << "mpi: " << Constants::mpi() << std::endl;
  std::cout << "h: " << Constants::h() << std::endl;
  std::cout << "hbar: " << Constants::hbar() << std::endl;
}

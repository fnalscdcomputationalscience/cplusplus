#include "useofbits.hh"

BitWord
set_with_macros(uint64_t reading,
                uint64_t count,
                uint64_t flag) {
  BitWord b {0};
  BW_SET_READING(b,reading);
  BW_SET_COUNT(b,count);
  BW_SET_FLAG(b,flag);
  return b;
}

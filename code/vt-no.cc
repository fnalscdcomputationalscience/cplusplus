#include <utility>

// One type per function signature is required.
struct OneArgSignal {
  typedef int (*func_t)(int);
  func_t f; // Stored callback.
  int invoke(int a) const
    { return f(a); }
};
struct TwoArgSignal {
  typedef
  int (*func_t)(int, long);
  func_t f; // Stored callback.
  int invoke(int a, long b) const
    { return f(a, b); }
};

int  f1(OneArgSignal s, int a)         { return s.invoke(a); }
long f2(TwoArgSignal s, int a, long b) { return s.invoke(a, b); }

#include <iostream>
#include <cmath>

using std::cout;
int main()
{
  OneArgSignal s1 { [](int x) { return 3+x; } };
  TwoArgSignal s2 { [](int x, long y) -> int { return x*y; } };
  cout << f1(s1, 7) << "\n";
  cout << f2(s2, 3, 4) << "\n";
}

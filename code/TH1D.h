#ifndef TH1D_H
#define TH1D_H

class TH1D
{
 public:
  TH1D(char const*, char const*, int, double, double);
  void Fill(double);
};

#endif

#include <vector>
#include <numeric>

double sum_1(std::vector<double> const& x) {
  double sum = 0.0;
  for (auto i = 0UL, sz = x.size(); i < sz; ++i) { sum += x[i]; }
  return sum;
}

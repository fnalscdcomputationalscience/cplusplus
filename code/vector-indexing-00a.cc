#include <vector>
#include <numeric>
//#pragma GCC diagnostic ignored "-Wsign-compare"
double sum_0a(std::vector<double> const& x) {
  double sum = 0.0;
  auto sz = x.size();
  for (int i = 0; i < sz; i++)
    { sum = sum + x[i]; }
  return sum;
}
#pragma GCC diagnostic pop

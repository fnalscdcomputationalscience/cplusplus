#include <vector>
#include <numeric>
// #pragma GCC diagnostic ignored "-Wsign-compare"
double sum_0(std::vector<double> const& x) {
  double sum = 0.0;
  for (int i = 0; i < x.size(); i++) { sum = sum + x[i]; }
  return sum;
}
#pragma GCC diagnostic pop

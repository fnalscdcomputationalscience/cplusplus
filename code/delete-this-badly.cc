struct X
{
  int value;
  ~X() noexcept;
};

X::~X() noexcept { delete this;}

int main()
{
  X a { 1 };
}

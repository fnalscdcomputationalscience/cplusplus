#include <cstdio>

int main() {
  // Most values are chosen to be ones that are NOT exactly
  // representable as binary floating-point numbers.
  double const d1 = 1.0/3.0;
  printf("d1: %a\n", d1);

  float const f1 = 1.0f/3.0f;
  printf("f1: %a\n", f1);

  float const f2 = 1.0/3.0;
  printf("f2: %a\n", f2);

  float const f3 = d1;
  printf("f3: %a\n", f3);

  double dz = 0.0;
  float fz = 0.0f;
  printf("zeros: %a %a\n", fz, dz);
}

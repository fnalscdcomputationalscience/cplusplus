#include <iostream>
#include <typeinfo>
#include <iomanip>

int main()
{
  bool t { true };
  bool f { false };
  auto x1 = t + t;
  auto x2 = t + f;
  auto x3 = f + t;
  auto x4 = f + f;

  bool b1 = t + t;
  bool b2 = t + f;
  bool b3 = f + t;
  bool b4 = f + f;

  std::cout << "x1: " << typeid(x1).name() << ' ' << x1 << ' ' << std::boolalpha << b1 << '\n';
  std::cout << "x2: " << typeid(x2).name() << ' ' << x2 << ' ' << b2 << '\n';
  std::cout << "x3: " << typeid(x3).name() << ' ' << x3 << ' ' << b3 << '\n';
  std::cout << "x4: " << typeid(x4).name() << ' ' << x4 << ' ' << b4 << '\n';

}

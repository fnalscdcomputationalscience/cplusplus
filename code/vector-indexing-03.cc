#include <vector>
#include <numeric>

double sum_3(std::vector<double> 
  const& x) {
  double sum = 0.0;
  for (auto val : x) sum += val;
  return sum;
}

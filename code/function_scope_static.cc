#include <string>

bool foo(bool b) {
  static bool value = b;
  return value;
}

bool bar(bool b) {
  return b;
}

std::string const& ff(std::string const& s) {
  static std::string value = s;
  return value;
}

std::string const& gg(std::string const& s) {
  return s;
}

std::string const& hh() {
  static const std::string s { "default" };
  return s;
}

std::string const& hh(std::string x) {
  static const std::string s { std::move(x) };
  return s;
}


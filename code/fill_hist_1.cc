#include <algorithm>

#include "TH1D.h"
#include "lambda.h"

void fill_hist_1(std::vector<double> const& nums, TH1D& h) {
  for (auto i=begin(nums), e=end(nums); i != e; ++i)
    { h.Fill(*i); }
}

#include <vector>
#include <numeric>

double sum_0(std::vector<double> const& x) {
  double sum = 0.0;
  for (int i = 0; i < x.size(); i++)
    { sum = sum + x[i]; }
  return sum;
}

double sum_0a(std::vector<double> const& x) {
  double sum = 0.0;
  auto sz = x.size();
  for (int i = 0; i < sz; i++)
    { sum = sum + x[i]; }
  return sum;
}

double sum_0b(std::vector<double> const& x) {
  double sum = 0.0;
  auto sz = x.size();
  for (int i = 0; i < sz; ++i)
    { sum = sum + x[i]; }
  return sum;
}

double sum_0c(std::vector<double> const& x) {
  double sum = 0.0;
  auto sz = x.size();
  for (auto i = 0UL; i < sz; ++i)
    { sum = sum + x[i]; }
  return sum;
}

double sum_1(std::vector<double> const& x) {
  double sum = 0.0;
  for (auto i = 0UL, sz = x.size(); i < sz; ++i)
    { sum += x[i]; }
  return sum;
}

double sum_2(std::vector<double> const& x) {
  double sum = 0.0;
  for (auto i = std::begin(x), e = std::end(x); i!=e; ++i)
    sum += *i;
  return sum;
}

double sum_3(std::vector<double> const& x) {
  double sum = 0.0;
  for (auto val : x) sum += val;
  return sum;
}

double sum_4(std::vector<double> const& x) {
  return std::accumulate(x.begin(), x.end(), 0.0);
}

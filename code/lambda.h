#ifndef LAMBDA_H
#define LAMBDA_H

#include <vector>

class TH1D;

void fill_hist_0(std::vector<double> const& nums, TH1D& h);
void fill_hist_0a(std::vector<double> const& nums, TH1D& h);
void fill_hist_1(std::vector<double> const& nums, TH1D& h);
void fill_hist_2(std::vector<double> const& nums, TH1D& h);
void fill_hist_3(std::vector<double> const& nums, TH1D& h);
void fill_hist_4(std::vector<double> const& nums, TH1D& h);


#endif //  LAMBDA_H

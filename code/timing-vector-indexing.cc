#include <array>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

double sum_0(std::vector<double> const& x)
{
  double sum = 0.0;
  for (int i = 0; i < x.size(); i++)
    sum = sum + x[i];
  return sum;
}

double sum_1(std::vector<double> const& x)
{
  double sum = 0.0;
  for (auto i = 0UL, sz = x.size(); i < sz; ++i)
    sum += x[i];
  return sum;
}

double sum_2(std::vector<double> const& x)
{
  double sum = 0.0;
  for (auto i = std::begin(x), e = std::end(x); i!=e; ++i)
    sum += *i;
  return sum;
}

double sum_3(std::vector<double> const& x)
{
  double sum = 0.0;
  for (auto val : x) sum += val;
  return sum;
}

double sum_4(std::vector<double> const& x)
{
  return std::accumulate(x.begin(), x.end(), 0.0);
}


typedef unsigned long long ull;

inline ull rdtsc()
{
  unsigned hi, lo;
  __asm__ __volatile__("rdtsc" : "=a"(lo), "=d"(hi));
  return ((unsigned long long)lo) | (((unsigned long long)hi) << 32);
}


void work(std::size_t rep, std::vector<double> vals)
{
  std::iota(vals.begin(), vals.end(), 1.0);

  std::array<double,5> results;
  std::array<ull, 6> times;
  
  times[0] = rdtsc();  
  results[0] = sum_0(vals);

  times[1] = rdtsc();
  results[1] = sum_1(vals);

  times[2] = rdtsc();
  results[2] = sum_2(vals);

  times[3] = rdtsc();
  results[3] = sum_3(vals);

  times[4] = rdtsc();
  results[4] = sum_4(vals);

  times[5] = rdtsc();

  for (std::size_t i = 0; i <=4; ++i)
    std::cout << rep << '\t' << i << '\t' << results[i] << '\t'  << times[i+1] - times[i] << std::endl;
}

int main(int argc, char** argv)
{
  if (argc != 3)
    {
      std::cerr << "Must specify a number of values to sum and number of times to repeat\n";
      return 1;
    }
  std::size_t nvals = std::stoll(argv[1]);
  std::size_t nreps  = std::stoll(argv[2]);

  std::vector<double> vals(nvals);
  iota(begin(vals), end(vals), 1.0);


  for ( std::size_t rep = 0; rep != nreps; ++rep)
    work(rep, vals);
}

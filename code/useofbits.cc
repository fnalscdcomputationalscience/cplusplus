
#include "useofbits.hh"
#include <iostream>
using namespace std;

int main()
{
  BitField a = set_with_bits(67ul, 2ul, 1ul);
  BitWord b = set_with_macros(67ul, 2ul, 1ul);

  BitWord ag = get_with_bits(a);
  BitWord bg = get_with_macros(b);

  cout << ag << " " << bg << "\n";
  return 0;
}

#include <list>

double sum_list(std::list<double> const& x) {
  double sum = 0.0;
  for (auto i = begin(x), e = end(x); i != e; ++i)
    { sum += *i; }
  return sum;
}

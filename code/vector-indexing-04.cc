#include <vector>
#include <numeric>

double sum_4(std::vector<double> const& x) 
  { return accumulate(begin(x), end(x), 0.0); }

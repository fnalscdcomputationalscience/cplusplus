struct Base
{
  virtual ~Base() = default;
  virtual int work() = 0;
};

struct D1 : Base
{
  int work() override;
};

struct D2 : Base
{
  int work() override;
};

int callwork(Base* p)
{
  return p->work();
}

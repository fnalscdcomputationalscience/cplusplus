#include <vector>
#include <numeric>

double sum_2(std::vector<double> const& x) {
  double sum = 0.0;
  for (auto i = begin(x), e = end(x); i!=e; ++i) { sum += *i; }
  return sum;
}


#include <algorithm>

#include "TH1D.h"
#include "lambda.h"

void fill_hist_2(std::vector<double> const& nums, TH1D& h) {
  for_each(begin(nums), end(nums), [&h](double x){ h.Fill(x); });
}
void fill_hist_3(std::vector<double> const& nums, TH1D& h) {
  auto fillhist = [&h](double x){ h.Fill(x); };
  std::for_each(nums.begin(), nums.end(), fillhist);
}


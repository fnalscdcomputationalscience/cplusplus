#include <cmath>
#include <iomanip>
#include <iostream>

// Note that static functions wrapping static constants (as opposed to
// class- and namespace-scope or global statics) is the only safe way to
// deal with the standard-specified inability to specify the
// initialization order of static variables across compilation
// units. Even that is only thread-safe with the new C++2011 memory
// model: under earlier standards more work would have been required.
namespace nm {
  struct Constants {
    static double mpi() { return M_PI; }
    static double h() { return 6.62606957e-34; }
    static double hbar();
    };
  }

inline
double
nm::Constants::hbar()
{
  static double const s_hbar = h() / (2.0 * mpi());
  return s_hbar;
}

int main() {
  using nm::Constants;
  std::cout << std::setprecision(12);
  std::cout << "mpi: " << Constants::mpi() << std::endl;
  std::cout << "h: " << Constants::h() << std::endl;
  std::cout << "hbar: " << Constants::hbar() << std::endl;
}

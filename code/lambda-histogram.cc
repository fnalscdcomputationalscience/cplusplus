#include <vector>
#include "TH1D.h"
#include "lambda.h"

#include <iostream>

int main()
{
  TH1D h1 { "", "", 100, -10.0, 10.0 };
  TH1D h2 { "", "", 100, -10.0, 10.0 };
  TH1D h3 { "", "", 100, -10.0, 10.0 };
  TH1D h4 { "", "", 100, -10.0, 10.0 };
  TH1D h5 { "", "", 100, -10.0, 10.0 };

  std::vector<double> numbers { 1.1, 2.2, 3.3 };

  fill_hist_0(numbers, h1);
  fill_hist_0a(numbers, h1);
  fill_hist_1(numbers, h1);
  fill_hist_2(numbers, h2);
  fill_hist_3(numbers, h3);
  fill_hist_4(numbers, h4);
}

#include "g5.hh"
#include <cmath>

double const constexpr mpi = 0x1.921fb54442d18p+1;

double
g5(double x, double mu, double sigma)
{
  double const z = (x - mu) / sigma;
  return std::exp(-z * z / 2.0) / std::sqrt(2. * mpi) / sigma;
}

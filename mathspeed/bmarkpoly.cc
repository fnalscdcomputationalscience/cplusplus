#include <benchmark/benchmark.h>
#include <cmath>

#include "poly.hh"

// Comparison between naive polynomial calculation and Horner form calculation


double volatile XGLOBAL = 0.23;

//---------------------------------------------------------------------

static void
BM_poly(benchmark::State& state)
{
  for (auto _ : state) {
    double res = poly(XGLOBAL);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_poly);

//---------------------------------------------------------------------
static void
BM_horner(benchmark::State& state)
{
  for (auto _ : state) {
    double res = horner(XGLOBAL);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_horner);

//----------------------------------------------------------------------
BENCHMARK_MAIN();

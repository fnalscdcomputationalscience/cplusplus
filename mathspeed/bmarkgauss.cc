#include <benchmark/benchmark.h>
#include <cmath>
#include "g5.hh"

double const constexpr mpi = 0x1.921fb54442d18p+1;
float const constexpr mpif = mpi;

double constexpr pi()
{
  return 4.0 * std::atan(1.0);
}

double volatile mu = 0.0;
double volatile sigma = 2.5;
double volatile x = 1.3;

float volatile muf = 0.0;
float volatile sigmaf = 2.5;
float volatile xf = 1.3;

double volatile A = 1.20;
double volatile B = 2.4;
double volatile C = 3.7;
double volatile M = 5.5;
double volatile z = 0.9;

//---------------------------------------------------------------------
inline double
g1(double x, double mu, double sigma)
{
  double const z = (x - mu) / sigma;
  return std::exp(-z * z / 2.0) / std::sqrt(2. * mpi) / sigma;
}

static void
BM_g1(benchmark::State& state)
{
  for (auto _ : state) {
    double res = g1(x, mu, sigma);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_g1);

//---------------------------------------------------------------------
inline double
g2(double x, double sigma)
{
  double const z = x / sigma;
  return std::exp(-z * z / 2.0) / std::sqrt(2. * mpi) / sigma;
}

static void
BM_g2(benchmark::State& state)
{
  for (auto _ : state) {
    double res = g2(x, sigma);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_g2);

//----------------------------------------------------------------------
inline float
g3(float x, float sigma)
{
  float const z = x / sigma;
  return std::exp(-z * z / 2.0) / std::sqrt(2. * mpif) / sigma;
}
static void
BM_g3(benchmark::State& state)
{
  for (auto _ : state) {
    float res = g3(xf, sigmaf);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_g3);

//----------------------------------------------------------------------

inline double
g4(double x, double sigma)
{
  double const z = x / sigma;
  return std::exp(-z * z / 2.0) / std::sqrt(2. * pi()) / sigma;
}
static void
BM_g4(benchmark::State& state)
{
  for (auto _ : state) {
    double res = g4(x, sigma);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_g4);

//----------------------------------------------------------------------

// function g5 is not inlined.
static void
BM_g5(benchmark::State& state)
{
  for (auto _ : state) {
    double res = g5(x, mu, sigma);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_g5);

//----------------------------------------------------------------------
inline double
p1(double A, double B, double C, double M, double z)
{
  return A * std::pow(M, B) * std::pow(1+z, C);
}

static void
BM_p1(benchmark::State& state)
{
  for (auto _ : state) {
    double res = p1(A, B, C, M, z);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_p1);

//----------------------------------------------------------------------
struct mz_power_law {
  double A_;
  double B_;
  double C_;
  double operator()(double M, double z) const {
    return A_ * std::pow(M, B_) * std::pow(1+z, C_);
  }
};

static void
BM_p2(benchmark::State& state)
{
  mz_power_law lambda { A, B, C };
  for (auto _ : state) {
    double res = lambda(M, z);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_p2);

//----------------------------------------------------------------------
struct mz_power_law2 {
  double lnA_;
  double B_;
  double C_;
  double operator()(double M, double z) const {
    double const lnres = B_ * std::log(M) + C_* std::log1p(z) + lnA_;
    return std::exp(lnres);
  }
};

static void
BM_p3(benchmark::State& state)
{
  mz_power_law2 lambda {std::log(A), B, C };
  for (auto _ : state) {
    double res = lambda(M, z);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_p3);

//----------------------------------------------------------------------
struct mz_power_law3 {
  double log2_A_;
  double B_;
  double C_;
  double operator()(double M, double z) const {
    double const log2_res = B_ * std::log2(M) + C_* std::log2(1+z) + log2_A_;
    return std::exp2(log2_res);
  }
};

static void
BM_p4(benchmark::State& state)
{
  mz_power_law3 lambda {std::log2(A), B, C };
  for (auto _ : state) {
    double res = lambda(M, z);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_p4);


//----------------------------------------------------------------------
BENCHMARK_MAIN();

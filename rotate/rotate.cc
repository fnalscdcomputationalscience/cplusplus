#include <algorithm>
#include <cmath>
#include <numeric>
#include <vector>
#include "range/v3/algorithm/rotate.hpp"
#include "benchmark/benchmark.h"

// Rotate the elements of the vector 'vals' by offset. Positive
// numbers shift left, negative numbers shift right.
template <class T>
void larsoft_rotate(std::vector<T>& vals, long offset) {
  if (labs(offset)>vals.size()) return;
  std::vector<T> temp;
  if (offset <= 0) {
    temp.assign(vals.end() + offset, vals.end());
    vals.erase(vals.end() + offset, vals.end());
    vals.insert(vals.begin(), temp.begin(), temp.end());
  } else {
    temp.assign(vals.begin(), vals.begin() + offset);
    vals.erase(vals.begin(), vals.begin() + offset);
    vals.insert(vals.end(), temp.begin(), temp.end());
  }
};

// Rotate the elements of the vector 'vals' by offset. Positive
// numbers shift left, negative numbers shift right.
template <class T>
void standard_rotate(std::vector<T>& vals, long offset) {
  if (labs(offset)>vals.size()) return;
  if (offset==0) return;
  if (offset < 0)
    std::rotate(vals.begin(), vals.end()+offset, vals.end());
  else
    std::rotate(vals.begin(), vals.begin()+offset, vals.end());
};

template <class T>
void my_rotate(std::vector<T>& vals, long offset) {
  if (labs(offset)>vals.size()) return;
  if (offset==0) return;
  if (offset <= 0) {
    std::vector<T> temp(vals.end() + offset, vals.end());
    vals.erase(vals.end() + offset, vals.end());
    vals.insert(vals.begin(), temp.begin(), temp.end());
  } else {
    std::vector<T> temp(vals.begin(), vals.begin() + offset);
    vals.erase(vals.begin(), vals.begin() + offset);
    vals.insert(vals.end(), temp.begin(), temp.end());
  }  
};

template <class T>
void range_rotate(std::vector<T>& vals, long offset) {
  if (labs(offset)>vals.size()) return;
  if (offset == 0) return;
  if (offset < 0) {
    ranges::rotate(vals.begin(), vals.end()+offset, vals.end());
  } else {
    ranges::rotate(vals.begin(), vals.begin()+offset, vals.end());
  }
}

auto generate = [](std::size_t n){
  std::vector<float> xs(n);
  std::iota(xs.begin(), xs.end(), 1);
  return xs;
};

static void BM_larsoft_rotate(benchmark::State& state) {
  auto const n = state.range(0);
  auto const m = state.range(1);
  auto vals = generate(n);
  
  while (state.KeepRunning()) {
    larsoft_rotate(vals, m);
  }
};

static void BM_standard_rotate(benchmark::State& state) {
  auto const n = state.range(0);
  auto const m = state.range(1);
  auto vals = generate(n);
  
  while (state.KeepRunning()) {
    standard_rotate(vals, m);
  }
};

static void BM_range_rotate(benchmark::State& state) {
  auto const n = state.range(0);
  auto const m = state.range(1);
  auto vals = generate(n);
  
  while (state.KeepRunning()) {
    range_rotate(vals, m);
  }
};

static void BM_my_rotate(benchmark::State& state) {
  auto const n = state.range(0);
  auto const m = state.range(1);
  auto vals = generate(n);
  
  while (state.KeepRunning()) {
    my_rotate(vals, m);
  }
};

BENCHMARK(BM_larsoft_rotate)->Ranges({{4, 4<<11}, {1,1<<10}});
BENCHMARK(BM_standard_rotate)->Ranges({{4, 4<<11}, {1,1<<10}});
BENCHMARK(BM_range_rotate)->Ranges({{4, 4<<11}, {1,1<<10}});
BENCHMARK(BM_my_rotate)->Ranges({{4, 4<<11}, {1,1<<10}});
BENCHMARK_MAIN();

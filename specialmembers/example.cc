#include "what_am_i.hh"
#include <iostream>
#include <string>

struct S
{
  std::string val;
  ~S() {};
};

int
main()
{
  std::cout << what::WhatAmI<S>();
}

#ifndef WHAT_AM_I_HH
#define WHAT_AM_I_HH

#include <iomanip>
#include <memory>
#include <ostream>
#include <type_traits>
#include <cxxabi.h>

template <class T>
std::string
type_name()
{
    typedef typename std::remove_reference<T>::type TR;
    std::unique_ptr<char, void(*)(void*)> own
           (abi::__cxa_demangle(typeid(TR).name(), nullptr,
                                           nullptr, nullptr),
                std::free
           );
    std::string r = own != nullptr ? own.get() : typeid(TR).name();
    if (std::is_const<TR>::value)
        r += " const";
    if (std::is_volatile<TR>::value)
        r += " volatile";
    if (std::is_lvalue_reference<T>::value)
        r += "&";
    else if (std::is_rvalue_reference<T>::value)
        r += "&&";
    return r;
}

namespace what {
  template <typename T>
  struct WhatAmI {
    using type = std::remove_cv_t<std::remove_reference_t<T>>;
  };

  template <typename T>
  std::ostream&
  operator<<(std::ostream& os, WhatAmI<T> const& t)
  {
    using type = typename WhatAmI<T>::type;
    os << "typename: " << type_name<T>();
    os << std::boolalpha;
    os << "\nis_void:                            " << std::is_void_v<type>
       << "\nis_null_pointer:                    " << std::is_null_pointer_v<type>
       << "\nis_integral:                        " << std::is_integral_v<type>
       << "\nis_floating_point:                  " << std::is_floating_point_v<type>
       << "\nis_array:                           " << std::is_array_v<type>
       << "\nis_enum:                            " << std::is_enum_v<type>
       << "\nis_union:                           " << std::is_union_v<type>
       << "\nis_class:                           " << std::is_class_v<type>
       << "\nis_function:                        " << std::is_function_v<type>
       << "\nis_pointer:                         " << std::is_pointer_v<type>
       << "\nis_lvalue_reference:                " << std::is_lvalue_reference_v<type>
       << "\nis_rvalue_reference:                " << std::is_rvalue_reference_v<type>
       << "\nis_member_object_pointer:           " << std::is_member_object_pointer_v<type>
       << "\nis_member_function_pointer:         " << std::is_member_function_pointer_v<type>
       << '\n'
       << "\nis_fundamental:                     " << std::is_fundamental_v<type>
       << "\nis_arithmetic:                      " << std::is_arithmetic_v<type>
       << "\nis_scalar:                          " << std::is_scalar_v<type>
       << "\nis_object:                          " << std::is_object_v<type>
       << "\nis_compound:                        " << std::is_compound_v<type>
       << "\nis_reference:                       " << std::is_reference_v<type>
       << "\nis_member_pointer:                  " << std::is_member_pointer_v<type>
       << '\n'
       << "\nis_trivial:                         " << std::is_trivial_v<type>
       << "\nis_trivially_copyable:              " << std::is_trivially_copyable_v<type>
       << "\nis_standard_layout:                 " << std::is_standard_layout_v<type>
       << "\nis_pod:                             " << std::is_pod_v<type>
       << "\nhas_unique_object_representations:  " << std::has_unique_object_representations<type>::value
       << "\nis_empty:                           " << std::is_empty_v<type>
       << "\nis_polymorphic:                     " << std::is_polymorphic_v<type>
       << "\nis_abstract:                        " << std::is_abstract_v<type>
       << "\nis_final:                           " << std::is_final_v<type>
       << "\nis_aggregate:                       " << std::is_aggregate_v<type>
       << "\nis_signed:                          " << std::is_signed_v<type>
       << "\nis_unsigned:                        " << std::is_unsigned_v<type>
       << '\n'
       << "\nis_default_constructible            " << std::is_default_constructible_v<type>
       << "\nis_trivially_default_constructible: " << std::is_trivially_default_constructible_v<type>
       << "\nis_nothrow_default_constructible:   " << std::is_nothrow_default_constructible_v<type>
       << "\nis_copy_constructible:              " << std::is_copy_constructible_v<type>
       << "\nis_trivially_copy_constructible:    " << std::is_trivially_copy_constructible_v<type>
       << "\nis_nothrow_copy_constructible:      " << std::is_nothrow_copy_constructible_v<type>
       << "\nis_move_constructible:              " << std::is_move_constructible_v<type>
       << "\nis_trivially_move_constructible:    " << std::is_trivially_move_constructible_v<type>
       << "\nis_nothrow_move_constructible:      " << std::is_nothrow_move_constructible_v<type>
       << "\nis_copy_assignable:                 " << std::is_copy_assignable_v<type>
       << "\nis_trivially_copy_assignable:       " << std::is_trivially_copy_assignable_v<type>
       << "\nis_nothrow_copy_assignable:         " << std::is_nothrow_copy_assignable_v<type>                  
       << "\nis_move_assignable:                 " << std::is_move_assignable_v<type>
       << "\nis_trivially_move_assignable:       " << std::is_trivially_move_assignable_v<type>
       << "\nis_nothrow_move_assignable:         " << std::is_nothrow_move_assignable_v<type>                  
       << "\nis_destructible:                    " << std::is_destructible_v<type>
       << "\nis_trivially_destructible:          " << std::is_trivially_destructible_v<type>
       << "\nis_nothrow_destructible:            " << std::is_nothrow_destructible_v<type>                  
       << "\nhas_virtual_destructor:             " << std::has_virtual_destructor_v<type>
       << "\nis_swappable:                       " << std::is_swappable_v<type>
       << "\nis_nothrow_swappable:               " << std::is_nothrow_swappable_v<type>         
       << '\n';
    return os;
  };
}; // namespace what
#endif

#include <benchmark/benchmark.h>
#include <memory>

//---------------------------------------------------------------------
// This benchmark compares the time required to invoke a function object
//   1. directly
//   2. via a bare pointer to the function object (no testing for nullity)
//   3. via a shared pointer to the function object (not testing for nullity)

// This is the function object we invoke. The multiplication is about the 
// fastest non-null operation we can perform.
struct myfunc
{
  explicit myfunc(double m) : multiplier(m) {};

  // note that the function call operator is available for inlining,
  // if the compiler decides to do so.
  double operator()(double x) const volatile { return multiplier * x; }  
  
  double const multiplier;
};

// x is the value of the argument we use. It is volatile to prevent the compiler
// from doing away with reading it on each access. It is const because we never
// change it through this code.

double const volatile x = 1.3;

//---------------------------------------------------------------------
static void
BM_inlinefunc(benchmark::State& state)
{
  myfunc f { 3.0 };
  for (auto _ : state) {
    double res = f(x);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_inlinefunc);

//---------------------------------------------------------------------
static void
BM_throughbareptr(benchmark::State& state)
{
  // We use volatile to prevent caching of the dereferenced shared_ptr.
  // We use const because we never modify it.
  myfunc volatile const * pf = new myfunc(3.0);
  for (auto _ : state) {
    double res = (*pf)(x);
    benchmark::DoNotOptimize(res);
  }
  delete pf;
}
BENCHMARK(BM_throughbareptr);

//---------------------------------------------------------------------
static void
BM_throughsharedptr(benchmark::State& state)
{
  // We use volatile to prevent caching of the dereferenced shared_ptr.
  // We use const because we never modify it.
  std::shared_ptr<myfunc> volatile const pf(new myfunc(3.0));
  for (auto _ : state) {
    std::shared_ptr<myfunc> const& cpf= const_cast<std::shared_ptr<myfunc> const&>(pf);
    double res = (*cpf)(x);
    benchmark::DoNotOptimize(res);
  }
}
BENCHMARK(BM_throughsharedptr);

BENCHMARK_MAIN();

#include "alignment.h"
#include <math.h>

alignment::alignment()
  : xOffset_(0.0), yOffset_(0.0), tilt_(0.0), cosTilt_(1.0), sinTilt_(0.0)
{}

alignment2::alignment2()
  : xOffset_(0.0), yOffset_(0.0), tilt_(0.0), cosTilt_(1.0), sinTilt_(0.0)
{}

alignment::alignment(double const& x, double const& y, double const& t)
  : xOffset_(x), yOffset_(y), tilt_(t), cosTilt_(cos(t)), sinTilt_(sin(t))
{}

alignment2::alignment2(double x, double y, double t)
  : xOffset_(x), yOffset_(y), tilt_(t), cosTilt_(cos(t)), sinTilt_(sin(t))
{}

alignment::alignment(alignment const& a)
  : xOffset_(a.xOffset_)
  , yOffset_(a.yOffset_)
  , tilt_(a.tilt_)
  , cosTilt_(a.cosTilt_)
  , sinTilt_(a.sinTilt_)
{}

alignment::~alignment() {}

bool
alignment::isNull() const
{
  return ((yOffset_ == 0.0) && (xOffset_ == 0.0) && (tilt_ == 0.0));
}

bool
alignment2::isNull() const
{
  return ((yOffset_ == 0.0) && (xOffset_ == 0.0) && (tilt_ == 0.0));
}

bool
alignment::operator==(alignment const& a) const
{
  return ((yOffset_ == a.yOffset_) && (xOffset_ == a.xOffset_) &&
          (tilt_ == a.tilt_));
}

bool
alignment2::operator==(alignment2 const& a) const
{
  return ((yOffset_ == a.yOffset_) && (xOffset_ == a.xOffset_) &&
          (tilt_ == a.tilt_));
}
alignment&
alignment::operator=(alignment const& a)
{

  if (&a == this)
    return *this;

  xOffset_ = a.xOffset_;
  yOffset_ = a.yOffset_;
  tilt_ = a.tilt_;
  cosTilt_ = a.cosTilt_;
  sinTilt_ = a.sinTilt_;

  return *this;
}

#ifndef ALIGNMENT__HH
#define ALIGNMENT__HH

class alignment {

public:
  alignment();
  alignment(double const& xOffset,
            double const& yOffset,
            double const& roll); // roll = tilt
  alignment(alignment const&);
  ~alignment();

  bool isNull() const;
  bool operator==(alignment const&) const;

  alignment& operator=(alignment const&);

  double const&
  x_offset()
  {
    return xOffset_;
  }
  double const&
  y_offset()
  {
    return yOffset_;
  }
  double const&
  roll()
  {
    return tilt_;
  }
  double const&
  cos_roll()
  {
    return cosTilt_;
  }
  double const&
  sin_roll()
  {
    return sinTilt_;
  }

private:
  double xOffset_; // offset in meters
  double yOffset_; // offset in meters
  double tilt_;    // roll in radians
                   // we could put in pitch someday
  double cosTilt_; // cos(tilt)
  double sinTilt_; // sin(tilt)
};

class alignment2 {

public:
  alignment2();
  alignment2(double xOffset, double yOffset,
             double roll); // roll = tilt

  bool isNull() const;
  bool operator==(alignment2 const&) const;

  double
  x_offset()
  {
    return xOffset_;
  }
  double
  y_offset()
  {
    return yOffset_;
  }
  double
  roll()
  {
    return tilt_;
  }
  double
  cos_roll()
  {
    return cosTilt_;
  }
  double
  sin_roll()
  {
    return sinTilt_;
  }

private:
  double xOffset_; // offset in meters
  double yOffset_; // offset in meters
  double tilt_;    // roll in radians
                   // we could put in pitch someday
  double cosTilt_; // cos(tilt)
  double sinTilt_; // sin(tilt)
};
#endif
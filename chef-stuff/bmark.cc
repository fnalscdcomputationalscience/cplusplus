#include <benchmark/benchmark.h>
#include "alignment.h"

static void BM_old_align(benchmark::State& state) {
  for (auto _ : state) {
    alignment a;
    alignment b(1.5, 2.5, 3.5);
    a = b;
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(b);
  }
}
// Register the function as a benchmark
BENCHMARK(BM_old_align);

// Define another benchmark
static void BM_new_align(benchmark::State& state) {
  for (auto _ : state) {
    alignment2 a;
    alignment2 b(1.5, 2.5, 3.5);
    a = b;
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(b);
  }
}
BENCHMARK(BM_new_align);

BENCHMARK_MAIN();
